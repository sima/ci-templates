# vim: set expandtab shiftwidth=2 tabstop=8 textwidth=0:
#
# THIS FILE IS GENERATED, DO NOT EDIT

# This template will create a arch image based on the following variables:
#
#  - ARCH_PKGS:    if set, list of packages that needs to be installed
#  - ARCH_EXEC:    if set, this command will be run once the packages have
#                    been installed
#  - UPSTREAM_REPO:  the upstream project on this gitlab instance where we might
#                    find the given tag (for example: `wayland/weston`)
#  - REPO_SUFFIX:    The repository name suffix after ".../arch/".
#                    If this variable isn't defined, "rolling" is used for
#                    the suffix.
#  - ARCH_TAG:     tag to copy the image from the upstream registry. If the
#                    tag does not exist, create a new build and tag it
#
# The resulting image will be pushed in the local registry, under:
#     $CI_REGISTRY_IMAGE/arch/$REPO_SUFFIX:$ARCH_TAG
#
# Two flavors of templates are available:
#   - `.arch@container-build`: this will force rebuild a new container
#     and tag it with $ARCH_TAG without checks
#   - `.arch@container-ifnot-exists`: this will rebuild a new container
#     only if $ARCH_TAG is not available in the local registry or
#     in the $UPSTREAM_REPO registry

# we can not reuse exported variables in after_script,
# so have a common definition
.arch_vars: &distro_vars |
        # exporting templates variables
        # https://gitlab.com/gitlab-com/support-forum/issues/4349
        export BUILDAH_FORMAT=docker
        export DISTRO=arch
        export DISTRO_TAG=$ARCH_TAG
        export DISTRO_VERSION=rolling
        export DISTRO_EXEC=$ARCH_EXEC
        if [ x"$REPO_SUFFIX" == x"" ] ;
        then
                export REPO_SUFFIX=$DISTRO_VERSION
        fi
        export BUILDAH_RUN="buildah run --isolation chroot"
        export BUILDAH_COMMIT="buildah commit --format docker"


.arch@container-build:
  image: $CI_REGISTRY/wayland/ci-templates/buildah:2020-02-19
  stage: build
  before_script:
    # log in to the registry
    - podman login -u $CI_REGISTRY_USER -p $CI_REGISTRY_PASSWORD $CI_REGISTRY

    - *distro_vars

  script:
  - *distro_vars
  - if [[ x"$DISTRO_TAG" == x"" ]] ;
    then
      echo $DISTRO tag missing;
      exit 1;
    fi
  - echo Building $DISTRO/$REPO_SUFFIX:$DISTRO_TAG from archlinux/base
    # initial set up: take the base image, update it and install the packages
  - buildcntr=$(buildah from archlinux/base)
  - buildmnt=$(buildah mount $buildcntr)

  - $BUILDAH_RUN $buildcntr pacman -S --refresh
  - $BUILDAH_RUN $buildcntr pacman -S --sysupgrade --noconfirm

  - if [[ x"$ARCH_PKGS" != x"" ]] ;
    then
      $BUILDAH_RUN $buildcntr pacman -S --noconfirm $ARCH_PKGS ;
    fi

    # check if there is an optional post install script and run it
  - if [[ x"$DISTRO_EXEC" != x"" ]] ;
    then
      echo Running $DISTRO_EXEC ;
      set -x ;
      mkdir $buildmnt/tmp/clone ;
      pushd $buildmnt/tmp/clone ;
      git init ;
      git remote add origin $CI_REPOSITORY_URL ;
      git fetch --depth 1 origin $CI_COMMIT_SHA ;
      git checkout FETCH_HEAD  > /dev/null;
      buildah config --workingdir /tmp/clone --env HOME="$HOME" $buildcntr ;
      $BUILDAH_RUN $buildcntr bash -c "set -x ; $DISTRO_EXEC" ;
      popd ;
      rm -rf $buildmnt/tmp/clone ;
      set +x ;
    fi

    # do not store the packages database, it's pointless
  - $BUILDAH_RUN $buildcntr mkdir -p /var/cache/pacman/pkg
  - $BUILDAH_RUN $buildcntr pacman -S --clean --noconfirm

    # set up the working directory
  - mkdir -p $buildmnt/app
  - buildah config --workingdir /app $buildcntr
    # umount the container, not required, but, heh
  - buildah unmount $buildcntr
    # tag the current container
  - $BUILDAH_COMMIT $buildcntr $CI_REGISTRY_IMAGE/$DISTRO/$REPO_SUFFIX:$DISTRO_TAG
    # clean up the working container
  - buildah rm $buildcntr

    # push the container image to the registry
    # There is a bug when pushing 2 tags in the same repo with the same base:
    # this may fail. Just retry it after.
  - export JOB_TAG="${DISTRO_TAG}-built-by-job-${CI_JOB_ID}"
  - podman push $CI_REGISTRY_IMAGE/$DISTRO/$REPO_SUFFIX:$DISTRO_TAG
                $CI_REGISTRY_IMAGE/$DISTRO/$REPO_SUFFIX:$JOB_TAG || true
  - sleep 2
  - podman push $CI_REGISTRY_IMAGE/$DISTRO/$REPO_SUFFIX:$DISTRO_TAG
                $CI_REGISTRY_IMAGE/$DISTRO/$REPO_SUFFIX:$JOB_TAG || true

    # Push the final tag
  - podman push $CI_REGISTRY_IMAGE/$DISTRO/$REPO_SUFFIX:$DISTRO_TAG || true
  - sleep 2
  - podman push $CI_REGISTRY_IMAGE/$DISTRO/$REPO_SUFFIX:$DISTRO_TAG


.arch@qemu-build:
  extends: .arch@container-build
  tags:
    - kvm
  image: $CI_REGISTRY/wayland/ci-templates/fedora/qemu-mkosi-base:2020-02-21
  script:
  - *distro_vars

  # start our current base mkosi image
  - /bin/bash /app/start_vm.sh -cdrom /app/my-seed.iso

  - QEMU_VERSION=rolling

  - |
    cat <<EOF > mkosi.default
    [Distribution]
    Distribution=arch
    Release=$QEMU_VERSION

    [Output]
    Format=gpt_ext4
    Bootable=yes
    BootProtocols=bios
    Password=root
    KernelCommandLine=!* selinux=0 audit=0 rw console=tty0 console=ttyS0

    [Partitions]
    RootSize=2G

    [Packages]
    # The packages to appear in both the build and the final image
    Packages=
      openssh
      networkmanager
      iproute
      iputils
      git
      sed
    EOF
  - echo $ARCH_PKGS | tr ' ' '\n' | sed -e 's/^/  /' >> mkosi.default

  # create a new ssh key
  - ssh-keygen -t rsa -f /root/.ssh/id_rsa_target -N ''
  - mkdir -p mkosi.extra/root/.ssh
  - chmod 700 mkosi.extra/root/.ssh
  - cp /root/.ssh/id_rsa_target.pub mkosi.extra/root/.ssh/authorized_keys
  - chmod 600 mkosi.extra/root/.ssh/authorized_keys

  # enable sshd on the target
  - mkdir -p mkosi.extra/etc/systemd/system/multi-user.target.wants

  # send the mkosi files to the VM
  - scp -P 5555 mkosi.default localhost:/root/mkosi.default
  - scp -P 5555 -r mkosi.extra localhost:/root/

  # enable sshd on the target
  - ssh localhost -p 5555 ln -s /usr/lib/systemd/system/sshd.service
                                mkosi.extra/etc/systemd/system/multi-user.target.wants/sshd.service

  # create a cache folder (useful only when manually testing this script)
  - ssh localhost -p 5555 mkdir mkosi.cache

  # run mkosi in the VM!
  - ssh localhost -p 5555 mkosi/mkosi

  # mount the root partition locally to extract the kernel and initramfs
  - ssh localhost -p 5555 mkdir loop
  - offset=$(ssh localhost -p 5555 fdisk -l image.raw | grep image.raw2 | cut -d ' ' -f 3)
  - ssh localhost -p 5555 mount -o ro,loop,offset=$(($offset * 512)) image.raw loop/

  # fetch kernel and initramfs
  - ssh localhost -p 5555 ls loop/boot/
  - ssh localhost -p 5555 "cp loop/boot/vmlinuz* loop/boot/initr* ."

  - ssh localhost -p 5555 umount loop/

  # now compress the image (we wanted to extract first the kernel and initrd)
  - ssh localhost -p 5555 xz -T0 image.raw

  # fetch the image and kernel
  - scp -P 5555 localhost:image.raw.xz dest-image.raw.xz

  - scp -P 5555 localhost:vmlinuz\* localhost:initr\* .

  # terminate qemu
  - kill $(pgrep qemu) || true

  # building the final image
  - |
    cat > /etc/containers/storage.conf <<EOF
    [storage]
    driver = "vfs"
    runroot = "/var/run/containers/storage"
    graphroot = "/var/lib/containers/storage"
    EOF

  - QEMU_BASE_IMAGE=${QEMU_BASE_IMAGE:-$CI_REGISTRY/wayland/ci-templates/fedora/qemu-base:2020-02-21}

  - echo Building $DISTRO/$REPO_SUFFIX:$DISTRO_TAG from $QEMU_BASE_IMAGE

  - buildcntr=$(buildah from $QEMU_BASE_IMAGE)
  - buildmnt=$(buildah mount $buildcntr)

  # insert our final VM image we just built
  - mkdir -p $buildmnt/app
  - mv dest-image.raw.xz $buildmnt/app/image.raw.xz
  - mv vmlinuz* initr* $buildmnt/app/
  - mkdir $buildmnt/root/.ssh
  - chmod 700 $buildmnt/root/.ssh
  - cp /root/.ssh/id_rsa_target $buildmnt/root/.ssh/id_rsa
  - cp /root/.ssh/id_rsa_target.pub $buildmnt/root/.ssh/id_rsa.pub

  - |
    cat > $buildmnt/app/start_vm_kernel.sh <<EOF
    #!/bin/bash

    set -x

    KERNEL=\$1
    shift

    set -e

    if [[ x"\$KERNEL" == x"" ]]
    then
      KERNEL=\$(ls /app/vmlinuz* | sort | tail -1)
    fi

    INITRD=\$(ls /app/initr* | sort | tail -1)

    bash /app/start_vm.sh -kernel \$KERNEL \\
                          -initrd \$INITRD \\
                          -append "root=/dev/sda2 selinux=0 audit=0 rw console=tty0 console=ttyS0" \\
                          "\$@"

    EOF
  - chmod +x $buildmnt/app/start_vm_kernel.sh

    # umount the container, not required, but, heh
  - buildah unmount $buildcntr

    # tag the current container
  - $BUILDAH_COMMIT $buildcntr $CI_REGISTRY_IMAGE/$DISTRO/$REPO_SUFFIX:$DISTRO_TAG

    # clean up the working container
  - buildah rm $buildcntr

    # push the container image to the registry
    # There is a bug when pushing 2 tags in the same repo with the same base:
    # this may fail. Just retry it after.
  - podman push $CI_REGISTRY_IMAGE/$DISTRO/$REPO_SUFFIX:$DISTRO_TAG || true
  - sleep 2
  - podman push $CI_REGISTRY_IMAGE/$DISTRO/$REPO_SUFFIX:$DISTRO_TAG

.before_script_ifnot_exists: &before_script_ifnot_exists
  before_script:
    # log in to the registry
    - podman login -u $CI_REGISTRY_USER -p $CI_REGISTRY_PASSWORD $CI_REGISTRY

    - *distro_vars

    # to be able to test the following script in the CI of the ci-templates
    # project itself, we need to put a special case here to have a
    # different image to pull if it already exists
    - export REPO_SUFFIX_LOCAL=$REPO_SUFFIX
    - if [[ x"$REPO_SUFFIX_LOCAL" == x"ci_templates_test_upstream" ]] ;
      then
        export REPO_SUFFIX=${DISTRO_VERSION} ;
      fi

    # check if our image is already in the current registry
    - skopeo inspect docker://$CI_REGISTRY_IMAGE/$DISTRO/$REPO_SUFFIX_LOCAL:$DISTRO_TAG > /dev/null && exit 0 || true
    # check if our image is already in the upstream registry
    - if [[ -z "$UPSTREAM_REPO" ]];
      then
        echo "WARNING! Variable \$UPSTREAM_REPO is undefined, cannot check for images" ;
      else
        skopeo inspect docker://$CI_REGISTRY/$UPSTREAM_REPO/$DISTRO/$REPO_SUFFIX:$DISTRO_TAG > /dev/null && touch .upstream || true ;
      fi
    # copy the original image into the current project registry namespace
    # we do 2 attempts with skopeo copy at most
    - if [ -f .upstream ] ;
      then
        skopeo copy --dest-creds $CI_REGISTRY_USER:$CI_REGISTRY_PASSWORD
                    docker://$CI_REGISTRY/$UPSTREAM_REPO/$DISTRO/$REPO_SUFFIX:$DISTRO_TAG
                    docker://$CI_REGISTRY_IMAGE/$DISTRO/$REPO_SUFFIX_LOCAL:$DISTRO_TAG ||
        skopeo copy --dest-creds $CI_REGISTRY_USER:$CI_REGISTRY_PASSWORD
                    docker://$CI_REGISTRY/$UPSTREAM_REPO/$DISTRO/$REPO_SUFFIX:$DISTRO_TAG
                    docker://$CI_REGISTRY_IMAGE/$DISTRO/$REPO_SUFFIX_LOCAL:$DISTRO_TAG ;

        exit 0 ;
      fi


.arch@container-ifnot-exists:
  extends: .arch@container-build
  <<: *before_script_ifnot_exists

.arch@qemu-ifnot-exists:
  extends: .arch@qemu-build
  <<: *before_script_ifnot_exists

