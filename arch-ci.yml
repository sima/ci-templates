# vim: set expandtab shiftwidth=2 tabstop=8 textwidth=0:
#
# THIS FILE IS GENERATED, DO NOT EDIT

variables:
  ARCH_PKGS: 'wget curl'
  ARCH_EXEC: '/bin/bash test/script.sh'


################################################################################
#
# Arch checks
#
################################################################################


#
# A few templates to avoid writing the image and stage in each job
#
.arch:ci@container-build:
  extends: .arch@container-build
  image: $CI_REGISTRY_IMAGE/buildah:$BOOTSTRAP_TAG
  stage: arch_container_build
  needs:
    - bootstrap
    - sanity check


.arch:ci@container-ifnot-exists:
  extends: .arch@container-ifnot-exists
  image: $CI_REGISTRY_IMAGE/buildah:$BOOTSTRAP_TAG
  stage: arch_container_build
  needs:
    - bootstrap
    - sanity check


#
# Qemu build
#
.arch:ci@qemu-build:
  extends: .arch@qemu-build
  image: $CI_REGISTRY_IMAGE/fedora/qemu-mkosi-base:$QEMU_TAG
  stage: arch_container_build
  artifacts:
    name: logs-$CI_PIPELINE_ID
    when: always
    expire_in: 1 week
    paths:
      - console.out
  needs:
    - bootstrap-qemu-mkosi
    - sanity check

#
# generic arch checks
#
.arch@check:
  stage: arch_check
  script:
      # run both curl and wget because one of those two is installed and one is
      # in the base image, but it depends on the distro which one
    - curl --insecure https://gitlab.freedesktop.org
    - wget --no-check-certificate https://gitlab.freedesktop.org
      # make sure our test script has been run
    - if [[ -e /test_file ]] ;
      then
        echo $ARCH_EXEC properly run ;
      else
        exit 1 ;
      fi


.arch@qemu-check:
  stage: arch_check
  tags:
    - kvm
  script:
    - pushd /app
      # start the VM
    - bash /app/start_vm.sh
      # run both curl and wget because one of those two is installed and one is
      # in the base image, but it depends on the distro which one
    - ssh -p 5555 localhost curl --insecure https://gitlab.freedesktop.org
    - ssh -p 5555 localhost wget --no-check-certificate https://gitlab.freedesktop.org
      # terminate the VM
    - ssh -p 5555 localhost halt -p || true
    - sleep 2
    - kill $(pgrep qemu) || true

      # start the VM, with the kernel parameters
    - bash /app/start_vm_kernel.sh
      # make sure we can still use curl/wget
    - ssh -p 5555 localhost curl --insecure https://gitlab.freedesktop.org
    - ssh -p 5555 localhost wget --no-check-certificate https://gitlab.freedesktop.org
      # terminate the VM
    - ssh -p 5555 localhost halt -p || true
    - sleep 2
    - kill $(pgrep qemu) || true
  artifacts:
    name: logs-$CI_PIPELINE_ID
    when: always
    expire_in: 1 week
    paths:
      - console.out


#
# straight arch build and test
#
arch:rolling@container-build:
  extends: .arch:ci@container-build
  variables:
    ARCH_TAG: $CI_PIPELINE_ID


arch:rolling@check:
  extends: .arch@check
  image: $CI_REGISTRY_IMAGE/arch/rolling:$CI_PIPELINE_ID
  needs:
    - arch:rolling@container-build
    - sanity check


arch:rolling@qemu-build:
  extends: .arch:ci@qemu-build
  variables:
    ARCH_TAG: qemu-$CI_PIPELINE_ID
    ARCH_PKGS: 'wget curl'
    QEMU_BASE_IMAGE: $CI_REGISTRY_IMAGE/fedora/qemu-base:$QEMU_TAG


arch:rolling@qemu-check:
  extends: .arch@qemu-check
  image: $CI_REGISTRY_IMAGE/arch/rolling:qemu-$CI_PIPELINE_ID
  needs:
    - arch:rolling@qemu-build
    - sanity check


#
# make sure we do rebuild the image if the tag does not exist and check
#
arch-forced:rolling@container-ifnot-exists:
  extends: .arch:ci@container-ifnot-exists
  variables:
    UPSTREAM_REPO: $CI_PROJECT_PATH
    ARCH_TAG: $CI_PIPELINE_IID


arch-forced-ifnot-exists:rolling@check:
  extends: .arch@check
  image: $CI_REGISTRY_IMAGE/arch/rolling:$CI_PIPELINE_IID
  needs:
    - arch-forced:rolling@container-ifnot-exists
    - sanity check


#
# make sure we do not rebuild the image if the tag exists (during the check)
#
arch:rolling@container-ifnot-exists:
  extends: .arch:ci@container-ifnot-exists
  stage: arch_check
  variables:
    UPSTREAM_REPO: $CI_PROJECT_PATH
    ARCH_TAG: $CI_PIPELINE_IID
    ARCH_PKGS: 'this-package-should-not-exist'
  needs:
    - arch-forced:rolling@container-ifnot-exists
    - sanity check


#
# make sure we do not rebuild the image if the tag exists in the upstream
# repository (during the check)
# special case where REPO_SUFFIX == ci_templates_test_upstream
#
arch:rolling-upstream@container-ifnot-exists:
  extends: .arch:ci@container-ifnot-exists
  stage: arch_check
  variables:
    UPSTREAM_REPO: $CI_PROJECT_PATH
    REPO_SUFFIX: ci_templates_test_upstream
    ARCH_TAG: $CI_PIPELINE_IID
    ARCH_PKGS: 'this-package-should-not-exist'
  needs:
    - arch-forced:rolling@container-ifnot-exists
    - sanity check
