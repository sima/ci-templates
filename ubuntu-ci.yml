# vim: set expandtab shiftwidth=2 tabstop=8 textwidth=0:
#
# THIS FILE IS GENERATED, DO NOT EDIT

variables:
  UBUNTU_DEBS: 'wget curl'
  UBUNTU_EXEC: '/bin/bash test/script.sh'


################################################################################
#
# Ubuntu checks
#
################################################################################


#
# A few templates to avoid writing the image and stage in each job
#
.ubuntu:ci@container-build:
  extends: .ubuntu@container-build
  image: $CI_REGISTRY_IMAGE/buildah:$BOOTSTRAP_TAG
  stage: ubuntu_container_build
  needs:
    - bootstrap
    - sanity check


.ubuntu:ci@container-ifnot-exists:
  extends: .ubuntu@container-ifnot-exists
  image: $CI_REGISTRY_IMAGE/buildah:$BOOTSTRAP_TAG
  stage: ubuntu_container_build
  needs:
    - bootstrap
    - sanity check


.ubuntu:ci@container-build@arm64v8:
  extends: .ubuntu@container-build@arm64v8
  image: $CI_REGISTRY_IMAGE/arm64v8/buildah:$BOOTSTRAP_TAG
  stage: ubuntu_container_build
  needs:
    - bootstrap@arm64v8
    - sanity check


.ubuntu:ci@container-ifnot-exists@arm64v8:
  extends: .ubuntu@container-ifnot-exists@arm64v8
  image: $CI_REGISTRY_IMAGE/arm64v8/buildah:$BOOTSTRAP_TAG
  stage: ubuntu_container_build
  needs:
    - bootstrap@arm64v8
    - sanity check

#
# Qemu build
#
.ubuntu:ci@qemu-build:
  extends: .ubuntu@qemu-build
  image: $CI_REGISTRY_IMAGE/fedora/qemu-mkosi-base:$QEMU_TAG
  stage: ubuntu_container_build
  artifacts:
    name: logs-$CI_PIPELINE_ID
    when: always
    expire_in: 1 week
    paths:
      - console.out
  needs:
    - bootstrap-qemu-mkosi
    - sanity check

#
# generic ubuntu checks
#
.ubuntu@check:
  stage: ubuntu_check
  script:
      # run both curl and wget because one of those two is installed and one is
      # in the base image, but it depends on the distro which one
    - curl --insecure https://gitlab.freedesktop.org
    - wget --no-check-certificate https://gitlab.freedesktop.org
      # make sure our test script has been run
    - if [[ -e /test_file ]] ;
      then
        echo $UBUNTU_EXEC properly run ;
      else
        exit 1 ;
      fi


.ubuntu@qemu-check:
  stage: ubuntu_check
  tags:
    - kvm
  script:
    - pushd /app
      # start the VM
    - bash /app/start_vm.sh
      # run both curl and wget because one of those two is installed and one is
      # in the base image, but it depends on the distro which one
    - ssh -p 5555 localhost curl --insecure https://gitlab.freedesktop.org
    - ssh -p 5555 localhost wget --no-check-certificate https://gitlab.freedesktop.org
      # terminate the VM
    - ssh -p 5555 localhost halt -p || true
    - sleep 2
    - kill $(pgrep qemu) || true

      # start the VM, with the kernel parameters
    - bash /app/start_vm_kernel.sh
      # make sure we can still use curl/wget
    - ssh -p 5555 localhost curl --insecure https://gitlab.freedesktop.org
    - ssh -p 5555 localhost wget --no-check-certificate https://gitlab.freedesktop.org
      # terminate the VM
    - ssh -p 5555 localhost halt -p || true
    - sleep 2
    - kill $(pgrep qemu) || true
  artifacts:
    name: logs-$CI_PIPELINE_ID
    when: always
    expire_in: 1 week
    paths:
      - console.out


#
# straight ubuntu build and test
#
ubuntu:19.10@container-build:
  extends: .ubuntu:ci@container-build
  variables:
    UBUNTU_VERSION: '19.10'
    UBUNTU_TAG: $CI_PIPELINE_ID


ubuntu:19.10@check:
  extends: .ubuntu@check
  image: $CI_REGISTRY_IMAGE/ubuntu/19.10:$CI_PIPELINE_ID
  needs:
    - ubuntu:19.10@container-build
    - sanity check


ubuntu:19.10@container-build@arm64v8:
  extends: .ubuntu:ci@container-build@arm64v8
  variables:
    UBUNTU_VERSION: '19.10'
    UBUNTU_TAG: arm64v8-$CI_PIPELINE_ID


ubuntu:19.10@check@arm64v8:
  extends: .ubuntu@check
  image: $CI_REGISTRY_IMAGE/ubuntu/19.10:arm64v8-$CI_PIPELINE_ID
  tags:
    - aarch64
  needs:
    - ubuntu:19.10@container-build@arm64v8
    - sanity check


ubuntu:19.10@qemu-build:
  extends: .ubuntu:ci@qemu-build
  variables:
    UBUNTU_VERSION: '19.10'
    UBUNTU_TAG: qemu-$CI_PIPELINE_ID
    UBUNTU_DEBS: 'wget curl'
    QEMU_BASE_IMAGE: $CI_REGISTRY_IMAGE/fedora/qemu-base:$QEMU_TAG


ubuntu:19.10@qemu-check:
  extends: .ubuntu@qemu-check
  image: $CI_REGISTRY_IMAGE/ubuntu/19.10:qemu-$CI_PIPELINE_ID
  needs:
    - ubuntu:19.10@qemu-build
    - sanity check


#
# make sure we do rebuild the image if the tag does not exist and check
#
ubuntu-forced:19.10@container-ifnot-exists:
  extends: .ubuntu:ci@container-ifnot-exists
  variables:
    UPSTREAM_REPO: $CI_PROJECT_PATH
    UBUNTU_VERSION: '19.10'
    UBUNTU_TAG: $CI_PIPELINE_IID


ubuntu-forced-ifnot-exists:19.10@check:
  extends: .ubuntu@check
  image: $CI_REGISTRY_IMAGE/ubuntu/19.10:$CI_PIPELINE_IID
  needs:
    - ubuntu-forced:19.10@container-ifnot-exists
    - sanity check


#
# make sure we do not rebuild the image if the tag exists (during the check)
#
ubuntu:19.10@container-ifnot-exists:
  extends: .ubuntu:ci@container-ifnot-exists
  stage: ubuntu_check
  variables:
    UPSTREAM_REPO: $CI_PROJECT_PATH
    UBUNTU_VERSION: '19.10'
    UBUNTU_TAG: $CI_PIPELINE_IID
    UBUNTU_DEBS: 'this-package-should-not-exist'
  needs:
    - ubuntu-forced:19.10@container-ifnot-exists
    - sanity check


#
# make sure we do not rebuild the image if the tag exists in the upstream
# repository (during the check)
# special case where REPO_SUFFIX == ci_templates_test_upstream
#
ubuntu:19.10-upstream@container-ifnot-exists:
  extends: .ubuntu:ci@container-ifnot-exists
  stage: ubuntu_check
  variables:
    UPSTREAM_REPO: $CI_PROJECT_PATH
    REPO_SUFFIX: ci_templates_test_upstream
    UBUNTU_VERSION: '19.10'
    UBUNTU_TAG: $CI_PIPELINE_IID
    UBUNTU_DEBS: 'this-package-should-not-exist'
  needs:
    - ubuntu-forced:19.10@container-ifnot-exists
    - sanity check

#
# Try our ubuntu scripts with other versions and check
#

ubuntu:18.04@container-build:
  extends: .ubuntu:ci@container-build
  variables:
    UBUNTU_VERSION: '18.04'
    UBUNTU_TAG: $CI_PIPELINE_ID

ubuntu:18.04@check:
  extends: .ubuntu@check
  image: $CI_REGISTRY_IMAGE/ubuntu/18.04:$CI_PIPELINE_ID
  needs:
    - ubuntu:18.04@container-build
    - sanity check
